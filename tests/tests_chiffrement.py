import unittest
from app import chiffrement

class TestChiffrementFunction(unittest.TestCase):
    def test_chiffrage(self):
        code1 = "Vive la Méthodologie"
        code2 = "Super Samy !"
        self.assertEqual(chiffrement(code1, 2), 'Xkxg"nc"Oëvjqfqnqikg')
        self.assertEqual(chiffrement(code2, 6), "Y{vkx&Ygs&'")

    def test_dechiffrage(self):
        code3 = "On aime les dinosaures"
        self.assertEqual(chiffrement(code1, -3), "Rq#dlph#ohv#glqrvdxuhv")