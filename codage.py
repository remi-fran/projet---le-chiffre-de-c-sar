from tkinter import *

def codage(string, offset, circulaire):
    #Cette fonction permet de chiffrer ou de déchiffrer un code (string) passé en paramètre.

    tableau_caractere = splitCode(string)
    string_sortie = ""
    for caractere in tableau_caractere:
        if caractere != '\n':
            if circulaire:
                caractere = codageCirculaire(ord(caractere)+offset)
                string_sortie = string_sortie + chr(caractere)
            else:
                string_sortie = string_sortie + chr(ord(caractere) + offset)
    return string_sortie

def codageCirculaire(caractere_fournis):
    """
    Cette fonction permet de gérer le codage circulaire, c'est à dire que lorsqu'on dépasse la 
    limite de notre alphabet on revient au début de celui-ci pour coder.
    """
    if caractere_fournis > 250: 
        caractere_fournis = caractere_fournis - 200
    if caractere_fournis < 50:
        caractere_fournis = caractere_fournis + 200
    return caractere_fournis

def getCode(text):
    #Cette fonction permet de récupérer dans un string le contenu de la zone de texte
    str = text.get("0.0", END)
    return str

def splitCode(str):
    #Cette fonction permet de retourner une liste constitué des caractère d'une chaine de caractères passée en paramètre
    return list(str)