from tkinter.messagebox import *
from tkinter import *
from codage import *

def buttonCoder():
    #Cette fonction permet l'appel de la fonction codage
    zone_text_resultat.delete('0.0', END)
    str = getCode(zone_text_saisie)
    resultat = codage(str, int(box_decalage.get()), variable.get())
    zone_text_resultat.insert(INSERT, resultat)

def buttonDecoder():
    zone_text_saisie.delete('0.0', END)
    str = getCode(zone_text_resultat)
    resultat = codage(str, -int(box_decalage.get()), variable.get())
    zone_text_saisie.insert(INSERT, resultat)

root = Tk()

label_saisie = Label(root, text = "Ecrire un code")
label_saisie.grid(column = 0, row = 0)
label_saisie = Label(root, text = "Ecrire un code pirate")
label_saisie.grid(column = 1, row = 0)

zone_text_resultat = Text(root)
zone_text_resultat.grid(column = 1, row = 1)
zone_text_saisie = Text(root)
zone_text_saisie.grid(column = 0, row = 1)

decodage_button=Button(root, text="Decodage", command=buttonDecoder)
decodage_button.grid(column = 1, row = 2, sticky='nsew')
codage_button=Button(root, text="Codage", command=buttonCoder)
codage_button.grid(column = 0, row = 2, sticky='nsew')

variable = IntVar()
checkbox_circulaire = Checkbutton(root, text="Codage de type circulaire", variable = variable)
checkbox_circulaire.grid(columnspan = 2, row = 4)

label_saisie = Label(root, text = "Décalage")
label_saisie.grid(columnspan=2, row=3)

box_decalage = Spinbox(root, from_=0, to=10)
box_decalage.grid(columnspan=2, row=4)

root.mainloop()